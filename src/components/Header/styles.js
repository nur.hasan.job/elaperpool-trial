import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export default StyleSheet.create({
  mainContainer: (transparent) => ({
    backgroundColor: transparent ? 'transparent' : 'blue',
    paddingTop: getStatusBarHeight(),
  }),
  itemContainer: {
    height: 50,
    alignItems: 'center',
    paddingHorizontal: 16,
    flexDirection: 'row',
  },
  textTitle: {
    color: 'white',
    lineHeight: 21,
    flex: 1,
  },
  containerButton: {
    marginRight: 16,
  },
  imageClose: {
    height: 16,
    width: 16,
  },
});
